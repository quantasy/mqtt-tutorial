# mqtt-tutorial

## step01
 As a first example it is shown how to
 
 * Access an MQTT-Broker as a client (connect)
 * send a message (publish) to a MQTT-Broker 
 * receive a message (subscribe) to MQTT-Broker 
 * disconnect form an MQTT-Broker

## step02
 This example demonstrates the 'retain' mechanism.
 If a new subscriber should always get the latest message then a publish can be 'retained'.
 This is indicated by the publisher.
 The implementation of the subscriber does not have to be changed.
 In order to demonstrate the 'retained' message, the subscriber prints out this information by accessing the message
 object.

## step03

This example demonstrates the 'cleanSession=false' mechanism.
Per default, if a subscriber loses connection, it will not get the messages
that have been provided meanwhile. It will restart at the 'retained' message
and will then keep on track with the publisher again.

But if messages shall be delivered to a subscriber, after it recuperates from
a 'black out', the subscriber can request the broker to store the messages
for him and deliver them once the publisher is back on again.

This is done by setting the connection to non-clean (setCleanSession to
false). This will alert the broker to store the messages in case the
subscriber loses the session. (here it is simulated by a System.exit(0) in
the stopReceivingTicks() method.

Please note that if a subscriber (is) unsubscribe(d)(s) from a topic, the
broker will not store the messages for this subscriber anymore.
 
CRITICAL: The mqttCallback must be ready to messageArrive prior to the connection!

### step03_1
This example demonstrates the 'cleanSession=false' mechanism (continued).<br/>
If a subscriber comes back online carrying the same connectionID, the broker will send all the stored data to this
connection.

As you can see here, the broker discriminates the different subscribers only by their connectionID, hence even a
different program can recuperate the stored data on the broker, as long as the connectionID matches
 
### step04
This example demonstrates the publisher's persistence ability.

If a publisher loses connection to the broker, it will not send any message and the broker cannot store them. <br/>
Hence, the publisher itself has to store those messages meanwhile and as soon as the connection to the broker is
established again, the messages will be sent to the broker.

This requires a persistence capability at the publisher's side (which is not always given).
As a side effect, if a new subscriber subscribes to the publisher's topic with cleanSession=false, some of the
historic data can be sent from the publisher via broker to the subscriber...


### step05
This example demonstrates how a subscriber can listen to multiple topics on a single connection.

If a subscriber wants to listen to multiple topics, it can register itself explicitly.

#### step05_1
This example demonstrates how a subscriber can listen to multiple topics via separate connections.

On multiple subscriptions different topics may be handled separately by using multiple connections. This way one
connection can be set to 'clean' whereas the other connection gets all data that has been sent during the time this
subscriber was unwillingly unavailable.

#### step05_2
This example demonstrates how a subscriber can listen to multiple topics on a single connection.

If a subscriber wants to listen to multiple topics, it can register itself implicitly using the recursive wildcard
'#'.

#### step05_3
This example demonstrates how a subscriber can listen to multiple topics on a single connection.

If a subscriber wants to listen to multiple topics, it can register itself implicitly using the 'level' wildcard '+'.

### step06
This example demonstrates the last will (testament).

If a client 'dies' unexpectedly, the testament will be opened and published as the last will by the broker at a specified topic.

In order to do so, the broker receives a testament by the client (during connection build up).
However, the message on the topic (for the last will) can be changed by the publisher at any time while it is still connected to the broker by
sending a new message.



This example demonstrates how a subscriber can listen to the publisher's last will.

The testament containing the last will is treated just like any other topic the subscriber is interested in.

The broker will publish the testament as soon as it realizes that the client 'dropped out of sight' , hence the
connection is lost without prior disconnect.


 
 
