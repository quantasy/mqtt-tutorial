/**
 *
 * @author Reto E. Koenig <reto.koenig@bfh.ch>
 */
package ch.quantasy.mqtt.tutorial.step05_1.agent;

import ch.quantasy.mqtt.tutorial.MqttTutorialEssentials;
import ch.quantasy.mqtt.tutorial.step05.service.TimeService;
import java.net.URI;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 *
 * @author Reto E. Koenig <reto.koenig@bfh.ch>
 */
public class TimeAgent implements MqttTutorialEssentials {

    private static final String CLIENT_ID;
    public static final URI SERVER_URI;
    public static final String CLIENT_ID_CLEAN;
    public static final String CLIENT_ID_HISTORY;

    public static final String TIME_SERVICE_TIME_5_TOPIC;
    public static final String TIME_SERVICE_TIME_3_TOPIC;

    static {
        SERVER_URI = URI.create(PROTOCOL + "://" + BROKER + ":" + PORT);
        CLIENT_ID = BASE_ID + "/ch.quantasy.mqtt.tutorial.TimeAgent.step05";

        CLIENT_ID_CLEAN = CLIENT_ID + ".clean";
        CLIENT_ID_HISTORY = CLIENT_ID + ".history";

        TIME_SERVICE_TIME_5_TOPIC = TimeService.TIME_SERVICE_TIME_5_TOPIC;
        TIME_SERVICE_TIME_3_TOPIC = TimeService.TIME_SERVICE_TIME_3_TOPIC;

    }

    private final MqttClient mqttClientClean;
    private final MqttClient mqttClientHistory;


    public TimeAgent() throws MqttException {
        mqttClientClean = new MqttClient(SERVER_URI.toString(), CLIENT_ID_CLEAN);
        mqttClientHistory = new MqttClient(SERVER_URI.toString(), CLIENT_ID_HISTORY);
    }

    @Override
    public MqttConnectOptions getConnectOptions() {
        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setUserName(USERNAME);
        mqttConnectOptions.setPassword(PASSWORD.toCharArray());

        return mqttConnectOptions;
    }

    public void connectToBroker() throws MqttException {
        MqttConnectOptions connectOptions=getConnectOptions();
        connectOptions.setCleanSession(true);
        mqttClientClean.setCallback(new MQTTMessageHandler());
        mqttClientClean.connect(connectOptions);
        
        connectOptions.setCleanSession(false);
        mqttClientHistory.setCallback(new MQTTMessageHandler());
        mqttClientHistory.connect(connectOptions);

    }

    public void disconnectFromBroker() throws MqttException {
        mqttClientClean.disconnect();
        mqttClientHistory.disconnect();
    }

    public void startReceivingTicks() throws MqttException {

        mqttClientClean.subscribe(TIME_SERVICE_TIME_5_TOPIC);
        mqttClientHistory.subscribe(TIME_SERVICE_TIME_3_TOPIC);

    }

    public void stopReceivingTicks() throws MqttException {
        mqttClientClean.setCallback(null);
        mqttClientHistory.setCallback(null);
        mqttClientClean.unsubscribe(TIME_SERVICE_TIME_5_TOPIC);
        mqttClientHistory.unsubscribe(TIME_SERVICE_TIME_3_TOPIC);
    }

    class MQTTMessageHandler implements MqttCallback {

        @Override
        public void connectionLost(Throwable thrwbl) {
            System.out.println("Connection Lost...");
        }

        @Override
        public void messageArrived(String string, MqttMessage mm) throws Exception {
            System.out.printf("Topic: (%s) Payload: (%s) Retained: (%b) \n", string, new String(mm.getPayload()), mm.isRetained());
        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken imdt) {
            System.out.println("Delivery Complete...");
        }
    }

}
