/**
 * This example demonstrates how a subscriber can listen to multiple topics via separate connections.<br/>
 * On multiple subscriptions different topics may be handled separately by using multiple connections. This way one
 * connection can be set to 'clean' whereas the other connection gets all data that has been sent during the time this
 * subscriber was unwillingly unavailable.
 *
 * @author Reto E. Koenig <reto.koenig@bfh.ch>
 */

package ch.quantasy.mqtt.tutorial.step05_1;
