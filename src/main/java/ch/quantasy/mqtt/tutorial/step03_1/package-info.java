/**
 * This example demonstrates the 'cleanSession=false' mechanism (continued).<br/>
 * If a subscriber comes back online carrying the same connectionID, the broker will send all the stored data to this
 * connection.<br/>
 * As you can see here, the broker discriminates the different subscribers only by their connectionID, hence even a
 * different program can recuperate the stored data on the broker, as long as the connectionID matches.<br/>
 * If the session is set to true, however, the broker will drop the stored
 * messages, without delivering them to the client.
 *
 * @author Reto E. Koenig <reto.koenig@bfh.ch>
 */

package ch.quantasy.mqtt.tutorial.step03_1;
