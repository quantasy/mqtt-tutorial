package ch.quantasy.mqtt.tutorial.step06.service;

import java.io.IOException;
import org.eclipse.paho.client.mqttv3.MqttException;

/**
 * This example demonstrates the last will (testament).<br/>
 * If a publisher 'dies' unexpectedly, a last will can be opened by the broker at a specified topic.<br/>
 * In order to do so, the broker receives a testament by the publisher (usually just after connection build up). <br/>
 * However, the message on the topic (for the last will) can be changed by the publisher at any time while it is still connected to the broker by
 * sending a new message. <br/>
 *
 * @author Reto E. Koenig <reto.koenig@bfh.ch>
 */
public class TimeServiceMain {

    public static void main(String[] args) throws MqttException, IOException {
	TimeService timeService = new TimeService();
	timeService.connectToBroker();
	timeService.startPublishingTicks();
	System.out.println("I am ticking...");
	System.out.println("Broker: " + TimeService.BROKER);
	System.out.println("Topic (Time 10 sec): " + TimeService.TIME_SERVICE_TIME_5_TOPIC);
	System.out.println("Topic (Time 3 sec): " + TimeService.TIME_SERVICE_TIME_3_TOPIC);
	System.out.println("Topic (Testament): " + TimeService.TIME_SERVICE_LASTWILL_TOPIC);

	System.out.println("...press a key to stop it.");
	System.in.read();
        //critical: If you disconnect gracefully, the testament will not be opened!
	//System.exit(0);
        timeService.stopPublishingTicks();
	timeService.disconnectFromBroker();
	System.out.println("Done.");
	
    }
}
