/**
 * This example demonstrates the last will (testament).<br/>
 * If a client 'dies' unexpectedly, a last will can be opened by the broker at a specified topic.<br/>
 * In order to do so, the broker receives a testament by the publisher (usually just after connection build up). <br/>
 * However, the message on the topic (for the last will) can be changed by the publisher at any time while it is still connected to the broker by
 * sending a new message. <br/>
 *
 * @author Reto E. Koenig <reto.koenig@bfh.ch>
 */

package ch.quantasy.mqtt.tutorial.step06;
