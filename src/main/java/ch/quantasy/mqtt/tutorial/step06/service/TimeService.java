package ch.quantasy.mqtt.tutorial.step06.service;

import ch.quantasy.mqtt.tutorial.MqttTutorialEssentials;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.time.Instant;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

/**
 *
 * @author Reto E. Koenig <reto.koenig@bfh.ch>
 */
public class TimeService implements MqttTutorialEssentials {

    public static final URI SERVER_URI;
    private static final String CLIENT_ID;

    public static final String TIME_SERVICE_TIME_5_TOPIC;
    public static final String TIME_SERVICE_TIME_3_TOPIC;
    public static final String TIME_SERVICE_LASTWILL_TOPIC;

    public static final String SERVICE_TYPE = "TimeService";
    public static final String SERVICE_INSTANCE = "step06";
    public static final String TIME_SERVICE_TIME_TOPIC;

    static {
        SERVER_URI = URI.create(PROTOCOL + "://" + BROKER + ":" + PORT);
        CLIENT_ID = BASE_ID + "/ch.quantasy.mqtt.tutorial.TimerService.step06";
        TIME_SERVICE_TIME_TOPIC = SERVICE_TYPE + "/" + SERVICE_INSTANCE + "/" + BASE_ID + "/" + "time";

        TIME_SERVICE_TIME_5_TOPIC = TIME_SERVICE_TIME_TOPIC + "/" + "delay/5/seconds";
        TIME_SERVICE_TIME_3_TOPIC = TIME_SERVICE_TIME_TOPIC + "/" + "delay/3/seconds";
        TIME_SERVICE_LASTWILL_TOPIC = SERVICE_TYPE + "/" + SERVICE_INSTANCE + "/" + "testament";
    }

    private final MqttClient mqttClient;

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private ScheduledFuture<?> ticker5;
    private ScheduledFuture<?> ticker3;

    public TimeService() throws MqttException {
        mqttClient = new MqttClient(SERVER_URI.toString(), CLIENT_ID, new MemoryPersistence());
    }

    @Override
    public MqttConnectOptions getConnectOptions() {
        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setUserName(USERNAME);
        mqttConnectOptions.setPassword(PASSWORD.toCharArray());
        mqttConnectOptions.setAutomaticReconnect(true);
        mqttConnectOptions.setWill(TIME_SERVICE_LASTWILL_TOPIC, "offline".getBytes(), 1, true);
        return mqttConnectOptions;
    }

    public void connectToBroker() throws MqttException {
        mqttClient.connect(getConnectOptions());
        mqttClient.publish(TIME_SERVICE_LASTWILL_TOPIC, "online".getBytes(), 1, true);
    }

    public void disconnectFromBroker() throws MqttException {
        mqttClient.publish(TIME_SERVICE_LASTWILL_TOPIC, "offline".getBytes(), 1, true);
        mqttClient.disconnect();
    }

    public void startPublishingTicks() {
        if (ticker5 != null) {
            return;
        }
        ticker5 = scheduler.scheduleAtFixedRate(new TimeTickManager(TIME_SERVICE_TIME_5_TOPIC), 0, 5, TimeUnit.SECONDS);
        ticker3 = scheduler.scheduleAtFixedRate(new TimeTickManager(TIME_SERVICE_TIME_3_TOPIC), 3, 3, TimeUnit.SECONDS);
    }

    public void stopPublishingTicks() {
        if (ticker5 != null) {
            ticker5.cancel(true);
            ticker5 = null;
        }
        if (ticker3 != null) {
            ticker3.cancel(true);
            ticker3 = null;
        }
    }

    class TimeTickManager implements Runnable {

        private final String topic;

        public TimeTickManager(String topic) {
            this.topic = topic;
        }

        @Override
        public void run() {
            try {
                Instant now = Instant.now();
                String nowString = now.toString();
                byte[] nowStringAsBytes = nowString.getBytes("UTF-8");
                MqttMessage message = new MqttMessage(nowStringAsBytes);
                message.setRetained(true);
                message.setQos(1);
                mqttClient.publish(topic, message);
            } catch (UnsupportedEncodingException | MqttException ex) {
                Logger.getLogger(TimeService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
