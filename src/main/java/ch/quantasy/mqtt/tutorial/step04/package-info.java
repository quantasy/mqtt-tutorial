/**
 *
 * This example demonstrates the publisher's persistence ability.<br/>
 * If a publisher is running but loses connection to the broker, it cannot send any message and the broker cannot store them. <br/>
 * Hence, the publisher itself has to store those messages meanwhile. As soon as the connection to the broker is
 * re-established, the messages will be sent to the broker.<br/>
 *
 * This requires a persistence capability at the publisher's side (which is not always given).<br/>
 * As a side effect, if a new subscriber subscribes to the publisher's topic with cleanSession=false, some of the
 * historic data can be sent from the publisher via broker to the subscriber...
 *
 * In order to demonstrate that, either the broker needs to be stopped
 * or the true connection has to fail. Then has to be established again.
 * 
 * BUT: Read the output of the Agent... carefully!!! Does it fit your expectation?
 * 
 * 
 * @author Reto E. Koenig <reto.koenig@bfh.ch>
 */

package ch.quantasy.mqtt.tutorial.step04;
