package ch.quantasy.mqtt.tutorial.step01.agent;

import ch.quantasy.mqtt.tutorial.step01.service.TimeService;
import java.net.URI;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import ch.quantasy.mqtt.tutorial.MqttTutorialEssentials;

/**
 *
 * @author Reto E. Koenig <reto.koenig@bfh.ch>
 */
public class TimeAgent implements MqttTutorialEssentials {

    private static final String CLIENT_ID;
    public static final URI SERVER_URI;

    static {
        SERVER_URI = URI.create(PROTOCOL + "://" + BROKER + ":" + PORT);
        CLIENT_ID = BASE_ID + "/ch.quantasy.mqtt.tutorial.TimeAgent.step01";
    }

    private final MqttClient mqttClient;

    public TimeAgent() throws MqttException {
        mqttClient = new MqttClient(SERVER_URI.toString(), CLIENT_ID, null);

    }

    @Override
    public MqttConnectOptions getConnectOptions() {
        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setUserName(USERNAME);
        mqttConnectOptions.setPassword(PASSWORD.toCharArray());
        return mqttConnectOptions;
    }

    public void connectToBroker() throws MqttException {
        mqttClient.connect(getConnectOptions());
    }

    public void disconnectFromBroker() throws MqttException {
        mqttClient.disconnect();
    }

    public void startReceivingTicks() throws MqttException {
        mqttClient.setCallback(new MQTTMessageHandler());
        mqttClient.subscribe(TimeService.TIME_SERVICE_TIME_TOPIC);

    }

    public void stopReceivingTicks() throws MqttException {
        mqttClient.setCallback(null);
        mqttClient.unsubscribe(TimeService.TIME_SERVICE_TIME_TOPIC);
    }

    class MQTTMessageHandler implements MqttCallback {

        @Override
        public void connectionLost(Throwable thrwbl) {
            System.out.println("Connection Lost...");
        }

        @Override
        public void messageArrived(String topic, MqttMessage message) throws Exception {
            System.out.printf("Topic: (%s) Payload: (%s) \n", topic, new String(message.getPayload()));
        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken imdt) {
            System.out.println("Delivery Complete...");
        }
    }

}
