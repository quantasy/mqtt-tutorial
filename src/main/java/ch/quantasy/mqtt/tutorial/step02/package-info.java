/**
 * This example demonstrates the 'retain' mechanism.<br/>
 * If a new subscriber should always get the latest message then a publish can be 'retained'.<br/>
 * This is indicated by the publisher.<br/>
 * The implementation of the subscriber does not have to be changed.<br/>
 * In order to demonstrate the 'retained' message, the subscriber prints out this information by accessing the message
 * object.
 *
 * @author Reto E. Koenig <reto.koenig@bfh.ch>
 */
package ch.quantasy.mqtt.tutorial.step02;
