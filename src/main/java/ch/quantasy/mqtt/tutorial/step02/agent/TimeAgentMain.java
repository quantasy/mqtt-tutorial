package ch.quantasy.mqtt.tutorial.step02.agent;

import ch.quantasy.mqtt.tutorial.step02.service.TimeService;
import java.io.IOException;
import org.eclipse.paho.client.mqttv3.MqttException;

/**
 * @author Reto E. Koenig <reto.koenig@bfh.ch>
 */
public class TimeAgentMain {

    public static void main(String[] args) throws MqttException, IOException {
	TimeAgent timeAgent = new TimeAgent();
	timeAgent.connectToBroker();

	System.out.println("I am going to receive ticks...");
	System.out.println("Broker: " + TimeAgent.BROKER);
	System.out.println("Topic (Time): " + TimeService.TIME_SERVICE_TIME_TOPIC);
	System.out.println("...press a key to end that.");
	timeAgent.startReceivingTicks();
	System.in.read();
	timeAgent.stopReceivingTicks();
	timeAgent.disconnectFromBroker();
	System.out.println("Done.");
	System.exit(0);
    }
}
