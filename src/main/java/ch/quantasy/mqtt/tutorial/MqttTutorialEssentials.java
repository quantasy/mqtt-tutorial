/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.quantasy.mqtt.tutorial;

import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;

/**
 *
 * @author reto
 */
public interface MqttTutorialEssentials {

    public static final String PROTOCOL = "tcp";
    public static final String BROKER = "<MQTT-Server-Address>";
    public static final String PORT = "1883";
    public static final String BASE_ID = getBaseName();
    public static final String USERNAME = "<username>";
    public static final String PASSWORD = "<password>";

    public MqttConnectOptions getConnectOptions();



    private static String getBaseName() {
        try {
            return java.net.InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ex) {
            Logger.getLogger(MqttTutorialEssentials.class.getName()).log(Level.SEVERE, null, ex);
            return "undefined";
        }
    }

}
