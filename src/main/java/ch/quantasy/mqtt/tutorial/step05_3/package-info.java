/**
 * This example demonstrates how a subscriber can listen to multiple topics at once by using the wildcard +.<br/>
 *
 * @author Reto E. Koenig <reto.koenig@bfh.ch>
 */

package ch.quantasy.mqtt.tutorial.step05_3;
