/**
 * This example demonstrates how a subscriber can listen to multiple topics on a single connection.<br/>
 * If a subscriber wants to listen to multiple topics, it can register itself explicitly.
 *
 * @author Reto E. Koenig <reto.koenig@bfh.ch>
 */

package ch.quantasy.mqtt.tutorial.step05;
