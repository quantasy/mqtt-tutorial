/**
 *
 * Publisher's persistence ability (continued)....<br/>
 * In the previous step... the time-stamps did not match the expected...
 * Instead of showing continuous counting... the time-stamps all showed 
 * almost the same time.
 * 
 * The reason is: Synchronous MQTT-Client.
 * The synchronous mqtt-client has a blocking publish method.
 * So, if the publish does not go through... the method will block,
 * and the timer is stalled...
 * 
 * Solution: Asynchronous MQTT-Client.
 * Using the asynchronous mqtt client, the publish-method immediately
 * comes back. In more complex projects, this is the way to go...
 * 
 * 
 * Now check the timings again!
 * 
 * 
 * In order to demonstrate that, either the broker needs to be stopped
 * or the true connection has to fail. Then has to be established again.
 * 
 * 
 * @author Reto E. Koenig <reto.koenig@bfh.ch>
 */

package ch.quantasy.mqtt.tutorial.step04_1;
