/**
 *
 * @author Reto E. Koenig <reto.koenig@bfh.ch>
 */
package ch.quantasy.mqtt.tutorial.step05_2.agent;

import ch.quantasy.mqtt.tutorial.MqttTutorialEssentials;
import ch.quantasy.mqtt.tutorial.step05.service.TimeService;

import java.net.URI;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 *
 * @author Reto E. Koenig <reto.koenig@bfh.ch>
 */
public class TimeAgent implements MqttTutorialEssentials {

    private static final String CLIENT_ID;
    public static final URI SERVER_URI;

    public static final String TIME_SERVICE_All_TOPICS;

    static {
        SERVER_URI = URI.create(PROTOCOL + "://" + BROKER + ":" + PORT);
        CLIENT_ID = BASE_ID + "/ch.quantasy.mqtt.tutorial.TimeAgent.step05";

        //The following is 'wrong'. You will get too much data! It is like a full dump of the Broker.
        //TIME_SERVICE_All_TOPICS = "#";

        //The following only subscribes to all topics beyond Time_Service_TIME_TOPIC
        TIME_SERVICE_All_TOPICS= TimeService.TIME_SERVICE_TIME_TOPIC + "/#";

    }

    private final MqttClient mqttClient;

    public TimeAgent() throws MqttException {
        mqttClient = new MqttClient(SERVER_URI.toString(), CLIENT_ID, null);
    }

    @Override
    public MqttConnectOptions getConnectOptions() {
        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setUserName(USERNAME);
        mqttConnectOptions.setPassword(PASSWORD.toCharArray());

        return mqttConnectOptions;
    }

    public void connectToBroker() throws MqttException {
        mqttClient.setCallback(new MQTTMessageHandler());

        mqttClient.connect(getConnectOptions());

    }

    public void disconnectFromBroker() throws MqttException {
        mqttClient.disconnect();
    }

    public void startReceivingTicks() throws MqttException {
        mqttClient.subscribe(TIME_SERVICE_All_TOPICS);

    }

    public void stopReceivingTicks() throws MqttException {
        mqttClient.setCallback(null);
        mqttClient.unsubscribe(TIME_SERVICE_All_TOPICS);
    }

    class MQTTMessageHandler implements MqttCallback {

        @Override
        public void connectionLost(Throwable thrwbl) {
            System.out.println("Connection Lost...");
        }

        @Override
        public void messageArrived(String string, MqttMessage mm) throws Exception {
            System.out.printf("Topic: (%s) Payload: (%s) Retained: (%b) \n", string, new String(mm.getPayload()), mm.isRetained());
        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken imdt) {
            System.out.println("Delivery Complete...");
        }
    }

}
